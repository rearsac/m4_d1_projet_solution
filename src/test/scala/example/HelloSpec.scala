package example

import org.scalatest._

class HelloSpec extends FlatSpec with Matchers {
  "Mon premier test pour afficher hello" should "me dire hello " in {
    Hello.sayHello() shouldEqual "Hello world"
  }
}
