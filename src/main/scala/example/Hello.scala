package example

object Hello extends App {
  lazy val greetings = "Hello world"

  def sayHello(): String  = {
    println(greetings)
    greetings
  }


  override def main(args: Array[String]): Unit = {
    sayHello()
  }
}